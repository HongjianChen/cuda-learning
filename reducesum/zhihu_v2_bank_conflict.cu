#include <iostream>
#include <cuda.h>
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"
#include "device_launch_parameters.h"
#include <time.h>
#include "tools.h"

#define THREAD_PER_BLOCK 256

// bank conflict
__global__ void reduce2(float* d_in, float* d_out) {
    __shared__ float sdata[THREAD_PER_BLOCK];

    // each thread loads one element from global to shared mem
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
    sdata[tid] = d_in[i];
    __syncthreads();

    // do reduction in shared mem
    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
        if (tid < s) {
            sdata[tid] += sdata[tid + s];
        }
        __syncthreads();
    }

    // write result for this block to global mem
    if (tid == 0) d_out[blockIdx.x] = sdata[0];
}

int main() {
    const int N = 32 * 1024 * 1024;
    // 计算需要的block数
    int block_num = N / THREAD_PER_BLOCK;
    long hstart, hend, dcstarthd, dcendhd, dcstartdh, dcenddh;

    float* a = (float*)malloc(N * sizeof(float));
    // 给host数组赋初值
    for (int i = 0; i < N; i++) {
        a[i] = 1;
    }
    // 把每个block中的数字累加，因此输出有block_num个数字
    float* out = (float*)malloc((N / THREAD_PER_BLOCK) * sizeof(float));
    float* ans = (float*)malloc((N / THREAD_PER_BLOCK) * sizeof(float));
    // host计算
    hstart = clock();
    // 计算出按block正确累加后的答案
    for (int i = 0; i < block_num; i++) {
        float cur = 0;
        for (int j = 0; j < THREAD_PER_BLOCK; j++) {
            cur += a[i * THREAD_PER_BLOCK + j];
        }
        ans[i] = cur;
    }
    hend = clock();

    // device计算
    float* d_a, * d_out;
    // host和device端分别申请一个长度为32*1024*1024的float数组，并返回host和device地址
    cudaMalloc((void**)&d_a, N * sizeof(float));
    cudaMalloc((void**)&d_out, (N / THREAD_PER_BLOCK) * sizeof(float));

    // 将host数组复制到device数组上
    dcstarthd = clock();
    cudaMemcpy(d_a, a, N * sizeof(float), cudaMemcpyHostToDevice);
    dcendhd = clock();

    // 设置Grid为(block_num, 1)，一维block
    dim3 Grid(N / THREAD_PER_BLOCK, 1);
    // 设置Block为(THREAD_PER_BLOCK)，一维thread
    dim3 Block(THREAD_PER_BLOCK, 1);

    // 启动recude2核函数
    cudaEvent_t start, stop;
    float dtime = 0.0;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    for (int i = 0; i < 10; i++) {
        reduce2 << <Grid, Block >> > (d_a, d_out);
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&dtime, start, stop);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    // 计算结果从device拷贝回host
    dcstartdh = time(NULL);
    cudaMemcpy(out, d_out, block_num * sizeof(float), cudaMemcpyDeviceToHost);
    dcenddh = time(NULL);

    if (check(out, ans, block_num)) printf("the ans is right\n");
    else {
        printf("the ans is wrong\n");
        for (int i = 0; i < block_num; i++) {
            printf("%lf ", out[i]);
        }
        printf("\n");
    }
    timeout(hstart, hend, dtime, dcstarthd, dcendhd, dcstartdh, dcenddh);

    cudaFree(d_a);
    cudaFree(d_out);
}