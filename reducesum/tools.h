#include <iostream>
using namespace std;

static bool check(float* out, float* res, int n) {
    for (int i = 0; i < n; i++) {
        if (out[i] != res[i])
            return false;
    }
    return true;
}


static void timeout (long hstart, long hend, float dtime, long dcstarthd, long dcendhd, long dcstartdh, long dcenddh) {
    cout << "host compute time: " << (float)(hend - hstart) << "ms" << endl;
    cout << "device compute time: " << dtime / 10 << "ms" << endl;
    cout << "memcopy time: " << (float)(dcendhd - dcstarthd + dcenddh - dcstartdh) << "ms" << endl;
    cout << "device total time: " << dtime / 10 + (float)(dcendhd - dcstarthd + dcenddh - dcstartdh) << "ms" << endl;
}