#include <iostream>
#include <cuda.h>
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"
#include "device_launch_parameters.h"
#include <time.h>
#include "tools.h"

#define THREAD_PER_BLOCK 256

__global__ void reduce0(float* d_in, float* d_out) {
    __shared__ float sdata[THREAD_PER_BLOCK];

    // 每个线程从全局内存中读取一个数据到共享内存中
    unsigned int tid = threadIdx.x;                         // 获取每个thread的x索引，因为thread是一维的，所以y始终为1
    unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // 通过thread的x索引计算出该数字在原始数组中的索引
    sdata[tid] = d_in[i];
    __syncthreads();

    // 在共享内存中计算reducesum
    for (unsigned int s = 1; s < blockDim.x; s *= 2) {
        if (tid % (2 * s) == 0) {
            sdata[tid] += sdata[tid + s]; // 每次循环将(2k + 1)s加到(2k)s上
        }
        __syncthreads(); // 所有线程每个循环都需要等待同步，然后进入下一次循环
    }

    // 所有线程累加到sdata[0]之后将每个block求和后的值传回全局内存
    if (tid == 0) d_out[blockIdx.x] = sdata[0];
}

int main() {
    const int N = 32 * 1024 * 1024;
    // 计算需要的block数
    int block_num = N / THREAD_PER_BLOCK;
    long hstart, hend, dcstarthd, dcendhd, dcstartdh, dcenddh;

    float* a = (float*)malloc(N * sizeof(float));
    // 给host数组赋初值
    for (int i = 0; i < N; i++) {
        a[i] = 1;
    }
    // 把每个block中的数字累加，因此输出有block_num个数字
    float* out = (float*)malloc((N / THREAD_PER_BLOCK) * sizeof(float));
    float* ans = (float*)malloc((N / THREAD_PER_BLOCK) * sizeof(float));
    // host计算
    hstart = clock();
    // 计算出按block正确累加后的答案
    for (int i = 0; i < block_num; i++) {
        float cur = 0;
        for (int j = 0; j < THREAD_PER_BLOCK; j++) {
            cur += a[i * THREAD_PER_BLOCK + j];
        }
        ans[i] = cur;
    }
    hend = clock();

    // device计算
    float* d_a, * d_out;
    // host和device端分别申请一个长度为32*1024*1024的float数组，并返回host和device地址
    cudaMalloc((void**)&d_a, N * sizeof(float));
    cudaMalloc((void**)&d_out, (N / THREAD_PER_BLOCK) * sizeof(float));

    // 将host数组复制到device数组上
    dcstarthd = clock();
    cudaMemcpy(d_a, a, N * sizeof(float), cudaMemcpyHostToDevice);
    dcendhd = clock();

    // 设置Grid为(block_num, 1)，一维block
    dim3 Grid(N / THREAD_PER_BLOCK, 1);
    // 设置Block为(THREAD_PER_BLOCK)，一维thread
    dim3 Block(THREAD_PER_BLOCK, 1);
    
    // 启动recude0核函数
    cudaEvent_t start, stop;
    float dtime = 0.0;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    for (int i = 0; i < 10; i++) {
        reduce0 << <Grid, Block >> > (d_a, d_out);
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&dtime, start, stop);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    // 计算结果从device拷贝回host
    dcstartdh = time(NULL);
    cudaMemcpy(out, d_out, block_num * sizeof(float), cudaMemcpyDeviceToHost);
    dcenddh = time(NULL);

    if (check(out, ans, block_num)) printf("the ans is right\n");
    else {
        printf("the ans is wrong\n");
        for (int i = 0; i < block_num; i++) {
            printf("%lf ", out[i]);
        }
        printf("\n");
    }
    timeout(hstart, hend, dtime, dcstarthd, dcendhd, dcstartdh, dcenddh);

    cudaFree(d_a);
    cudaFree(d_out);
}